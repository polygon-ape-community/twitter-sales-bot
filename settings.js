const abiJSON = require('./abi.json');
require('dotenv').config();

const contract = {
    address: process.env.PROJECT_CONTRACT_ADDRESS,
    abi: abiJSON
}
const alchemy = {
    websocket: process.env.ALCHEMY_WEBSOCKET_URL
}
const currencies = {
    '0x7ceb23fd6bc0add59e62ac25578270cff1b9f619' : {
        'name': 'WETH',
        'symbol': 'Ξ',
        'decimals': 18,
        'threshold': 1
    },
    '0xd4945a3d0de9923035521687d4bf18cc9b0c7c2a' : {
        'name': 'Luxy',
        'symbol': '$Luxy',
        'decimals': 18,
        'threshold': 1
    }
};
const marketplaces = {
    '0xf715beb51ec8f63317d66f491e37e7bb048fcc2d' : {
        'name': 'OpenSea 🌊',
        'site': 'https://opensea.io/assets/matic/',
        'twitter_handle': 'opensea',
        'log_decoder': [
            {
                type: 'bytes32',
                name: 'orderHash'
            },
            {
                type: 'uint256',
                name: 'orderNonce',
            },
            {
                type: 'address',
                name: 'currency',
            },
            {
                type: 'address',
                name: 'collection',
            },
            {
                type: 'uint256',
                name: 'tokenId',
            },
            {
                type: 'uint256',
                name: 'amount',
            },
            {
                type: 'uint256',
                name: 'unkown',
            },
            {
                type: 'uint',
                name: 'price'
            }
        ]
    },
    '0x21c90bce2943dd2b706248e906d4df9637be27c4' : {
        'name': 'Luxy',
        'site': 'https://beta.luxy.io/collection/',
        'twitter_handle': 'Luxy_io',
        'log_decoder': [
            {
                type: 'bytes32',
                name: 'orderHash'
            },
            {
                type: 'uint256',
                name: 'orderNonce',
            },
            {
                type: 'address',
                name: 'currency',
            },
            {
                type: 'address',
                name: 'collection',
            },
            {
                type: 'uint256',
                name: 'tokenId',
            },
            {
                type: 'uint256',
                name: 'price',
            },
            {
                type: 'uint256',
                name: 'unkown',
            },
            {
                type: 'uint',
                name: 'amount'
            }
        ]
    },
    '0x535a8a4a0d1eb5e9541227ebfb13d98c67c0fbf1' : {
        'name': 'BitKeep',
        'site': 'https://opensea.io/assets/matic/',
        'twitter_handle': 'BitKeepOS',
        'log_decoder': [
            {
                type: 'uint256',
                name: 'unknown',
            },
            {
                type: 'uint256',
                name: 'unknown',
            },
            {
                type: 'uint',
                name: 'price'
            }
        ]
    },
    '0x7bc8b1b5aba4df3be9f9a32dae501214dc0e4f3f' : {
        'name': 'Tofu NFT',
        'site': 'https://tofunft.com/nft/polygon/',
        'twitter_handle': 'tofuNFT',
        'log_decoder': [
            {
                type: 'uint256',
                name: 'unknown',
            },
            {
                type: 'uint256',
                name: 'unknown',
            },
            {
                type: 'uint',
                name: 'unknown'
            },
            {
                type: 'uint',
                name: 'price'
            }
        ]
    }
};
const events = {
    transfer_event_types: [
        "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef", // Transfer
    ],
    sale_event_types: [
        "0x6869791f0a34781b29882982cc39e882768cf2c96995c2a110c577c53bc932d5",
        "0xb445ff9394b12ee126e355ecf23f382077c812db983045e7d7243794daf9acf5",
        "0xc4109843e0b7d514e4c093114b863f8e7d8d9a458c372cd51bfe526b588006c9",
        "0x5beea7b3b87c573953fec05007114d17712e5775d364acc106d8da9e74849033"
    ],
    mint_event_types: [
        "0xe6497e3ee548a3372136af2fcb0696db31fc6cf20260707645068bd3fe97f3c4"
    ]
}
const twitter = {
    consumer_key:process.env.CONSUMER_KEY,
    consumer_secret:process.env.CONSUMER_SECRET,
    access_token:process.env.ACCESS_TOKEN_KEY,
    access_token_secret:process.env.ACCESS_TOKEN_SECRET
}

module.exports = {
    settings: {
        currencies: currencies,
        marketplaces: marketplaces,
        contract:contract,
        alchemy:alchemy,
        twitter:twitter,
        events:events
    }
};
